#include <stdio.h>
#include <string.h>
#include "marathon.h"

int main(int argc, char *argv[]) {
    char *marathon_url = NULL;
    
    int argn;
    for (argn = 1; argn < argc; argn++) {
        if ((strcmp (argv [argn], "--help") == 0)
        || (strcmp (argv [argn], "-h") == 0)) {
            puts ("example  [options] ...");
            puts ("  --marathon / -m    url for marathon service");
            return 0;
        }

        else
        if ((strcmp (argv [argn], "--marathon") == 0)
        || (strcmp (argv [argn], "-m") == 0)) {
            argn++;
            marathon_url = argv [argn];
        }
    }

    if (!marathon_url) {
        printf ("--marathon is required\n");
        return 1;
    }

    GoString marathonUrl = {marathon_url, strlen (marathon_url)};
    GoString match = {"logtalez", 8};
    char *endpoints = DiscoverEndpoints(marathonUrl, match);
    printf("%s\n", endpoints);
    return 0;
}

