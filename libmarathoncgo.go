package main

import "C"

import (
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	marathon "github.com/gambol99/go-marathon"
)

const (
	respTimeout = 10
	dialTimeout = 10
	keepAlive   = 10
)

func newMarathonClient(marathonURL string) (marathon.Marathon, error) {
	config := marathon.NewDefaultConfig()
	config.URL = marathonURL
	config.HTTPClient = &http.Client{
		Timeout: (time.Duration(respTimeout) * time.Second),
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   dialTimeout * time.Second,
				KeepAlive: keepAlive * time.Second,
			}).Dial,
		},
	}

	client, err := marathon.NewClient(config)
	return client, err
}

//export DiscoverEndpoints
func DiscoverEndpoints(url, key string) *C.char {

	client, err := newMarathonClient(url)
	if err != nil {
		return C.CString("")
	}

	applications, err := client.Applications(nil)
	if err != nil {
		return C.CString("")
	}

	var endpoints []string
	for _, application := range applications.Apps {
		details, err := client.Application(application.ID)
		if err != nil {
			return C.CString("")
		}

		for i, pd := range *details.PortDefinitions {
			if pd.Name == key {
				tasks, err := client.Tasks(application.ID)
				if err != nil {
					return C.CString("")
				}

				for _, task := range tasks.Tasks {
					endpoints = append(endpoints, fmt.Sprintf("tcp://%s:%d", task.Host, task.Ports[i]))
				}
			}
		}
	}

	endpointsCSV := strings.Join(endpoints, ",")
	return C.CString(endpointsCSV)
}

func main() {}
