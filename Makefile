default:
	go build -o libmarathoncgo.so -buildmode=c-shared libmarathoncgo.go

example:
	${CC} -o example example.c -lmarathoncgo

install:
	install ./libmarathoncgo.so /usr/local/lib/libmarathoncgo.so
	install ./libmarathoncgo.h /usr/local/include/libmarathoncgo.h
	install ./libmarathoncgo.pc /usr/local/lib/pkgconfig/libmarathoncgo.pc

clean:
	rm -f libmarathoncgo.so
	rm -f libmarathoncgo.h
	rm -f example
